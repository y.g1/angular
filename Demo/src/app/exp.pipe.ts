import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exp'
})
export class ExpPipe implements PipeTransform {

  experience: any;
  currentyear: any;
  joinYear:any;

  transform(value: any): any {
   
     this.currentyear = new Date().getFullYear();
     this.joinYear = new Date(value).getFullYear();
     this.experience = this.currentyear - this.joinYear;


    return this.experience;
  }

}
