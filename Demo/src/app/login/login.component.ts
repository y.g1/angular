import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
//Import Router class
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emailId: any;
  password: any;
  employees: any;
  emp: any;
 //Dependency Injection for EmpService, Router
 

  constructor(private router: Router, private toastr: ToastrService,private service: EmpService) {

    this.employees = [
    {empId:1,empName:'Ganesh',salary:10000.00,gender:'Male',doj:'2010-10-20',country:'INDIA',emailId:'ganesh@gmail.com',password:'123'},
    {empId:2,empName:'Rakesh',salary:20000.00,gender:'Male',doj:'2013-11-05',country:'INDIA',emailId:'rakesh@gmail.com',password:'123'},
    {empId:3,empName:'Rupa',salary:30000.00,gender:'Female',doj:'2018-12-23',country:'INDIA',emailId:'rupa@gmail.com',password:'123'},
    {empId:4,empName:'Vishnu',salary:40000.00,gender:'Male',doj:'2020-01-18',country:'INDIA',emailId:'vishnu@gmail.com',password:'123'},
   ];
  }

  ngOnInit() {
  }

  submit() {
    console.log("EmailId : " + this.emailId);
    console.log("Password: " + this.password);
  }

  loginSubmit(loginForm: any) {
    console.log(loginForm);
    console.log("EmailId : " + loginForm.emailId);
    console.log("Password: " + loginForm.password);

    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {

      this.service.setIsUserLoggedIn();

      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
    } else {
      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {

        this.service.setIsUserLoggedIn();

        localStorage.setItem("emailId", loginForm.emailId);
        this.router.navigate(['products']);
      } else {

        this.toastr.error('Invalid Credentials', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 3000, // 3 seconds
        });
        return;
      }
    }
    this.toastr.success('Login Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });
  }
}