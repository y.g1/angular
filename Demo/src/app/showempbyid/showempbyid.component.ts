import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {
  
  employees: any;
  empId: any;
  emp: any;

  constructor() {
    this.employees = [
      {empId:1,empName:'Ganesh',salary:10000.00,gender:'Male',doj:'2010-10-20',country:'INDIA',emailId:'ganesh@gmail.com',password:'123'},
      {empId:2,empName:'Rakesh',salary:20000.00,gender:'Male',doj:'2013-11-05',country:'INDIA',emailId:'rakesh@gmail.com',password:'123'},
      {empId:3,empName:'Rupa',salary:30000.00,gender:'Female',doj:'2018-12-23',country:'INDIA',emailId:'rupa@gmail.com',password:'123'},
      {empId:4,empName:'Vishnu',salary:40000.00,gender:'Male',doj:'2020-01-18',country:'INDIA',emailId:'vishnu@gmail.com',password:'123'},
      ];
  }

  ngOnInit() {
  }

  getEmpById(employee: any) {
    this.emp = null;
   
    this.employees.forEach((element: any) => {
      if (element.empId == employee.empId) {
        this.emp = element;
      }
    });
  }
  

}