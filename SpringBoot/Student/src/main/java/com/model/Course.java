package com.model;

public class Course {
	private int CourseId;
	private String CourseName;
	private double fee;
	
	public Course() {
		super();
	}

	public Course(int CourseId, String CourseName, double fee) {
		super();
		this.CourseId = CourseId;
		this.CourseName = CourseName;
		this.fee = fee;
	}

	public int getCourseId() {
		return CourseId;
	}
	public void setCourseId(int CourseId) {
		this.CourseId = CourseId;
	}

	public String getCourseName() {
		return CourseName;
	}
	public void setCourseName(String CourseName) {
		this.CourseName = CourseName;
	}

	public double getfee() {
		return fee;
	}
	public void setfee(double fee) {
		this.fee = fee;
	}

	@Override
	public String toString() {
		return "Course [CourseId=" + CourseId + ", CourseName=" + CourseName + ", fee=" + fee + "]";
	}	
}