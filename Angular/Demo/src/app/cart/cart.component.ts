import { Component, OnInit, createPlatformFactory } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  
  cartItems: any;
  cartProducts: any;

  constructor() {
  }
  
  ngOnInit() {    
    this.cartProducts = localStorage.getItem("cartItems");
    this.cartItems = JSON.parse(this.cartProducts);
  }

}

